class DiskspacesController < ApplicationController

    include Sys

    def index

        # free disk spaces, count from root "/"
        stat = Filesystem.stat("/")
        # puts @stat.to_yaml

        @bytes = stat.bytes_free
        @kilo_bytes = @bytes.to_kb
        @mega_bytes = @bytes.to_mb
        @giga_bytes = @bytes.to_gb
        @tera_bytes = @bytes.to_tb

        # percent used
        @percent_used = stat.percent_used


        
        # disk usage, count from "path_to_rails_root/public/uploads"
        # https://ruby-doc.org/stdlib-3.1.1/libdoc/open3/rdoc/Open3.html
        # not available on Windows, using command "du" (unix based)
        if not OS.windows?
            public_uploads_dir = Rails.root.join('public', 'uploads')
            
            stdout, stderr, status = Open3.capture3("du -sh " + public_uploads_dir.to_s)
            @public_uploads_size = stdout.split("\t")[0]
        end

    end

end
